# Contributing
This project have some contributing policies, that we want to be followed.

# How to write javadoc
Javadoc must start by having a brief explanation of what the method/class does and telling some advices if needed (like the difference of *print* and *println*), and a block telling the main code of the method (if there are a method that not are so important for the method working, you can omit him in this part), and you can also simplify the code (for example, there is a `forEach` working at some point, but you can replace him for e common `for` loop).