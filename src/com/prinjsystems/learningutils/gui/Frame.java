/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */
package com.prinjsystems.learningutils.gui;

import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * This class handles Swing operations, creating components and JFrame itself,
 * and processing then.
 */
public class Frame {
	private JFrame frame;
	private List<Component> components;
	
	/**
	 * Default constructor for Frame class.
	 */
	public Frame() {
		components = new ArrayList<>();
	}
	
	/**
	 * Calls constructor of {@code JFrame} class, and defines it size.
	 * <p>
	 * This method works by using the following code:
	 * <pre>
	 * {@code
	 * frame = new JFrame(title);
	 * frame.setSize(width, height);
	 * frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	 * frame.setLocationRelativeTo(null);
	 * }
	 * </pre>
	 * Where the first line constructs frame object, with {@code title} parameter, of type {@code JFrame},
	 * the second line defines the size of {@code frame}, with {@code width} and {@code height} parameter,
	 * the third defines that your application will exit if there are no more operations or frames to run,
	 * and the fourth line make the frame appears at middle of screen.
	 * @param title Title of the frame (text at header).
	 * @param width Horizontal size of screen (not counting screen borders and header).
	 * @param height Vertical size of screen (not counting screen borders and header).
	 */
	public void createFrame(String title, int width, int height) {
		frame = new JFrame(title);
		frame.setSize(width, height);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null); // Make things more easier for a learning developer
	}
	/**
	 * Adds all components to frame and make it visible.
	 * <p>
	 * This method works by using the following code:
	 * <pre>
	 * {@code
	 * for(Component c : components) {
	 *     frame.add(c);
	 * }
	 * frame.setVisible(true);
	 * }
	 * </pre>
	 * The first line is the header of a {@code for} loop, that iterates over a list
	 * containing all components.
	 * The second line adds the current component to the frame.
	 * The last line makes the frame visible.
	 */
	public void start() {
		if(frame.isVisible()) {
			throw new RuntimeException("Frame already started!");
		}
		
		JPanel contentPane = new JPanel(true); /* Activating double buffering, the user
												  doesn`t need to care about screen flickering */
		components.stream().forEach((c) -> contentPane.add(c));
		
		contentPane.setBounds(0, 0, frame.getWidth(), frame.getHeight());
		frame.setContentPane(contentPane);
		
		frame.setVisible(true);
	}
	
	/**
	 * Adds a component to the component list.
	 * The component listed here can be any component class ({@code JPanel}, {@code JLabel},
	 * {@code JButton}, etc).
	 * <p>
	 * This method works by using the code below:
	 * <pre>
	 * {@code
	 * components.add(c);
	 * }
	 * </pre>
	 * This line adds the component in parameter to the list of components.
	 * @param c 
	 */
	public void addComponent(Component c) {
		components.add(c);
	}
	
	/**
	 * Stops all actions of the frame.
	 * <p>
	 * This method uses the following code:
	 * {@code frame.dispose()}
	 * <p>
	 * The method {@code dispose()} of {@code frame} exits it from actions and
	 * visibility.
	 */
	public void stop() {
		if(!frame.isVisible()) {
			throw new RuntimeException("Frame not started!");
		}
		
		frame.dispose();
	}
}
