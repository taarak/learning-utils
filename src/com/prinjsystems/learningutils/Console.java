/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */
package com.prinjsystems.learningutils;

import java.util.Scanner;

/**
 * A utilities class for handling console operations, like text input/output.
 */
public class Console {
	private static Scanner scanner;
	
	static {
		scanner = new Scanner(System.in);
	}
	
	/**
	 * Prints the given text parameter into screen, <strong>without</strong> a new line ({@code \n} character).
	 * <p>
	 * This method works by using the folowing command:
	 * {@code System.out.print(text)}, that prints a string in the screen without a new line at end.
	 * @param text Text parameter to print at screen (prints at current caret/cursor position).
	 */
	public static void print(String text) {
		System.out.print(text);
	}
	
	/**
	 * Prints the given text parameter into screen, <strong>with</strong> a new line ({@code \n} character).
	 * <p>
	 * This method works by using the folowing command:
	 * {@code System.out.println(text)}, that prints a string in the screen with a new line at end.
	 * @param text Text parameter to print at screen (prints at current caret/cursor position).
	 */
	public static void println(String text) {
		System.out.println(text);
	}
	
	/**
	 * Reads a string (line), that will only end when {@code Enter} character is activated.
	 * This method is the most recommended over all operations, since it is the only one that
	 * accepts the enter character, being more user-friendly and failure-clear method.
	 * <p>
	 * This method works by using the folowing command:
	 * {@code scanner.nextLine()}, where {@code scanner} is a instance of the
	 * {@code Scanner} class.
	 * @return Readed string before Enter character is activated.
	 */
	public static String readString() {
		return scanner.nextLine();
	}
	
	/**
	 * Reads a {@code long} input. Method readString with a conversion from {@code String}
	 * to {@code long} is more recommended.
	 * <p>
	 * This method works by using the folowing command:
	 * {@code scanner.nextLong()}, where {@code scanner} is a instance of the
	 * {@code Scanner} class.
	 * @return Readed long from input.
	 */
	public static long readLong() {
		return scanner.nextLong();
	}
	
	/**
	 * Reads a {@code int} input. Method readString with a conversion from {@code String}
	 * to {@code int} is more recommended.
	 * <p>
	 * This method works by using the folowing command:
	 * {@code scanner.nextInt()}, where {@code scanner} is a instance of the
	 * {@code Scanner} class.
	 * @return Readed int from input.
	 */
	public static int readInteger() {
		return scanner.nextInt();
	}
	
	/**
	 * Reads a {@code float} input. Method readString with a conversion from {@code String}
	 * to {@code float} is more recommended.
	 * <p>
	 * This method works by using the folowing command:
	 * {@code scanner.nextFloat()}, where {@code scanner} is a instance of the
	 * {@code Scanner} class.
	 * @return Readed float from input.
	 */
	public static float readFloat() {
		return scanner.nextFloat();
	}
	
	/**
	 * Reads a {@code double} input. Method readString with a conversion from {@code String}
	 * to {@code double} is more recommended.
	 * <p>
	 * This method works by using the folowing command:
	 * {@code scanner.nextDouble()}, where {@code scanner} is a instance of the
	 * {@code Scanner} class.
	 * @return Readed double from input.
	 */
	public static double readDouble() {
		return scanner.nextDouble();
	}
	
	/**
	 * Reads a {@code boolean} input. Method readString with a conversion from {@code String}
	 * to {@code boolean} is more recommended.
	 * <p>
	 * This method works by using the folowing command:
	 * {@code scanner.nextBoolean()}, where {@code scanner} is a instance of the
	 * {@code Scanner} class.
	 * @return Readed boolean from input.
	 */
	public static boolean readBoolean() {
		return scanner.nextBoolean();
	}
	
	/**
	 * Reads a {@code short} input. This method is not recommended in use with
	 * general purpose console applications, and is only created to be used for
	 * testing purpose or a very specific needing.
	 * <p>
	 * This method works by using the folowing command:
	 * {@code scanner.nextShort()}, where {@code scanner} is a instance of the
	 * {@code Scanner} class.
	 * @return Readed short from input.
	 */
	public static short readShort() {
		return scanner.nextShort();
	}
	
	/**
	 * Reads a {@code byte} input. This method is not recommended in use with
	 * general purpose console applications, and is only created to be used for
	 * testing purpose or a very specific needing.
	 * <p>
	 * This method works by using the folowing command:
	 * {@code scanner.nextByte()}, where {@code scanner} is a instance of the
	 * {@code Scanner} class.
	 * @return Readed byte from input.
	 */
	public static byte readByte() {
		return scanner.nextByte();
	}
	
	/**
	 * Closes all connections with input channels (but output channels remains
	 * working).
	 * <p>
	 * This method works with the following code:
	 * <p>
	 * {@code scanner.close()}, thats closes the connection of scanner.
	 */
	public static void end() {
		scanner.close();
		scanner = null; // Defined to null to make exit() method working more easy
	}
	
	/**
	 * Exits from JVM with a exit code.
	 * <p>
	 * This method works with the following code:
	 * <pre>
	 * {@code
	 * if(scanner != null) {
	 *     end();
	 * }
	 * System.exit(exitCode);
	 * }
	 * </pre>
	 * If the method {@code end()} not is called in this session, it calls it,
	 * and then exits the JVM with {@code exitCode} as exit code.
	 * @param exitCode JVM exit code. If this code is different from zero, is
	 * considered that the JVM exited because an error.
	 */
	public static void exit(int exitCode) {
		if(scanner != null) {
			end();
		}
		System.exit(exitCode);
	}
}
