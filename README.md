# Learning Utils
This is a library focused on the people that is in learning process of the programming language Java (but also have some very minimal knowledge).

It has self-explanatory methods, and it javadoc design is created to help the growing developer to know what the method really does, and tell some tips about what he want to do, and if he has a better way to do this.

## How to use the platform
If you are using any IDE to start developing your code, you will need to download the last stable release of LearningUtils library (the one with the highest code, like: 1.5.3 is a higher version than 1.5.1, and search for "stable" keyword at the jar file), and import it to the created project with the libraries section of your IDE.

## What if I see an error?
The first thing you should do is to check if your code is correct (if there is no warnings or errors in your code, show by your IDE, or any other problems that you see). If you checked the code, and didn't find anything wrong, you can create a issue at BitBucket, and I can try to help you.